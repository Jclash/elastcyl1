
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <math.h>


#include <sstream> // Required for stringstreams


std::string itos ( int number ){
  std::ostringstream oss;
  oss<< number;
  return oss.str();
} 

int save_vector(double* A, int N, const char* fname){
  using namespace std;
  ofstream out(fname,ios::out);

  string fname_str = fname;

  if(!out.fail()){
    for(int i = 0; i < N; i++) out << A[i] << endl;
    cout << "file " << fname_str << " was successfully created" << endl;
  }
  else{
    cout << "error creating output file" << endl;
  }
  
  out.close();
  
  return 0;
}

int save_grafic_2d_on01(double* A, int N, const char* fname){
  using namespace std;
  ofstream out(fname,ios::out);

  string fname_str = fname;

  if(!out.fail()){
    for(int i = 0; i < N; i++) out << (1.0*i)/(N - 1) <<" "<< A[i] << endl;
    cout << "file " << fname_str << " was successfully created" << endl;
  }
  else{
    cout << "error creating output file" << endl;
  }
  
  out.close();
  
  return 0;
}

int hyper_1d(){
  using namespace std;

  double *U0, *V0, *U1, *U2, *Ut;

  double T = 5.5;

  int M = 10;
  
  double h = 1.0/M;
  double tau = h/2;

  int N = T/tau;

  

  U0 = new double[M+1];
  U1 = new double[M+1];
  U2 = new double[M+1];
  V0 = new double[M+1];
  Ut = new double[M+1];

  cout<<"h = "<<h<<endl;
  cout<<"tau = "<<tau<<endl;

  // Initial conditions
  for(int i = 0; i <= M; i++) U0[i] = 0.0;//sin(M_PI/M*i);
  for(int i = 0; i <= M; i++){
    if (( i >= M/4) && (i <= 3*M/4)) 
      V0[i] = -2.0;
    else 
      V0[i] = 0.0;
  }

  U1[0] = 0.0;
  for(int i = 1; i < M; i++) U1[i] = U0[i];
  U1[M] = 0.0;

  U2[0] = 0.0;
  for(int i = 1; i < M; i++) U2[i] = U1[i] + tau*V0[i] + 1.0/2.0*(tau/h)*(tau/h)*(U1[i + 1] - 2*U1[i] + U1[i - 1]);
  U2[M] = 0.0;

  save_grafic_2d_on01(U1,M+1,"step0.txt");
  save_grafic_2d_on01(U2,M+1,"step1.txt");

  for(int k = 2; k < N; k++){

    for(int i = 1; i < M; i++) Ut[i] = 2*U2[i] - U1[i] + (tau/h)*(tau/h)*(U2[i + 1] - 2*U2[i] + U2[i - 1]);  

    for(int i = 1; i < M; i++) U1[i] = U2[i];

    for(int i = 1; i < M; i++) U2[i] = Ut[i];

    string fname_str = "step" + itos(k) + ".txt";

//    if( k == 7) save_vector(U2,M+1,"step7.txt");

    save_grafic_2d_on01(U2,M+1,fname_str.c_str());
    
  }

  save_grafic_2d_on01(U2,M+1,"stepN.txt");

  delete[](U0);
  delete[](U1);
  delete[](U2);
  delete[](V0);

  return 0;
}

int main(){
  using namespace std;

  cout<<"Hello world!"<<endl;

  hyper_1d();  

  return 0;
}
