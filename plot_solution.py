import matplotlib 
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import cm               # colormaps

import numpy as np
#import Tkinter
import sys



k = -1
loading = True

while loading:

    k = k + 1

    stepnum = k
    stepname = str(stepnum)
    fname = "step" + stepname

    loaded = False

    try:
	infname = fname + ".txt"
	x, u = np.loadtxt(infname, unpack=True)
	loaded = True
    except:
	if k == 0:
	    err_msg = "Could not load data from file %s." % infname \
              + " Did you forget to run the program?"
	    raise Exception(err_msg)
	else:
	    print "%d steps were loaded" % k
	    loaded = False
	
    if (loaded == False):
	break

    #print u
    #print x

    plt.clf()

    plt.axis([0,1,-1.2,1.2])
    plt.plot(x, u, 'yo-')

    pltfname = "plot_" + fname + ".png"
    plt.savefig(pltfname)
    print 'Plot saved as %s' % pltfname

