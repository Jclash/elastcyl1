CC = g++
CFLAGS = 
LCLAGS = $(CFLAGS)

.PHONY: plots, clean, clobber

%.o : %.cpp
	$(CC) $(CFLAGS) -c $<

elastcyl1: main.o 
	$(CC) $(LCLAGS) main.o -o elastcyl1

solution.txt: elastcyl1
	@echo 
	@echo Running code...
	rm -f step*.txt
	./elastcyl1
	>solution.txt

plots: solution.txt
	@echo 
	@echo Plotting results...
	rm -f plot*.png
	python plot_solution.py
	>plots

animate: plots
	@echo 
	@echo Plotting results...
	#python animate_solution.py
	rm -f solution.avi
	./ffmpeg -framerate 25 -r 3 -i plot_step%d.png -vcodec mpeg4 solution.avi

clean:
	rm -f *.o elastcyl1

clobber: clean
	rm -f solution.txt *.png

resclean:
	rm -f *.png step*.txt *.avi solution.txt plots